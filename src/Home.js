import React from 'react'

import './home.css'

const Home = () => {
    return (
        <div className={'home'}>
            <iframe
                title={'360 image'}
                allowFullScreen="true" 
                allow="accelerometer; magnetometer; gyroscope" 
                src="https://panoraven.com/en/embed/tgw19tVkpu">
            </iframe>
        </div>
    )
}

export default Home


